from typing import List, Dict, Callable, Union, Sequence, MutableSequence

import pandas as pd
import numpy as np
import feather

import pymc3 as pm
from theano import tensor as T

from .utilities import make_survival_dataframe, make_strata, strata
from .analysis import df_summary_pereira
from .covariates import CovariateSet, DataSet, CovariateBuilder
from .basemodel import BaseModel
from .result import Result


class BayesianSurvival:
    """Perform Bayesian survival analysis using PyMC3
    Parameters
    ----------
    df: pd.DataFrame
        Source data for endpoint observations and covariates"""

    def __init__(self,
                 df: pd.DataFrame,
                 name_suffix: str = ""):
        self.data = df
        self.name_suffix = name_suffix

    def multivariate_models(self,
                            survival_columns_list: Sequence[Sequence[str]],
                            covariates_with_distribution: Dict[str, Callable] = None,
                            covariates_fully_observed: Sequence[str] = None,
                            strata_columns: Sequence[str] = None,
                            model_type: str="piecewise_exponential",
                            ref_level_dict: Dict = None) -> Dict[str, "BayesianSurvival.Model"]:
        """Creates a multivariate model for each endpoint in survival_columns_list

        Parameters: see multivariate_model
        :returns: A dict of survival_columns[0] -> Model
        """
        return {
            survival_columns[0]:
            self.multivariate_model(survival_columns,
                                    covariates_with_distribution,
                                    covariates_fully_observed,
                                    strata_columns,
                                    model_type,
                                    ref_level_dict)
            for survival_columns in survival_columns_list
        }

    def multivariate_model(self,
                           survival_columns: Sequence[str],
                           covariates_with_distribution: Dict[str, Callable] = None,
                           covariates_fully_observed: Sequence[str] = None,
                           strata_columns: Sequence[str] = None,
                           model_type: str="piecewise_exponential",
                           ref_level_dict: Dict = None):
        """Create a  bayesian multivariate survival model

        :param survival_columns:
            A list of two column names of time and status indicator (0/False: censored, 1/True: event occurred)
        :param covariates_with_distribution:
            The covariates for which missing values are compensated by defining a distribution.
            A dict of column name -> Callable, or None.
            the callable shall return a pm.Distribution subclass and have the signature
            (data: pd.DataFrame, name, observed)
            The data parameter is the current full data (missing values possibly filtered) for your internal purposes.
            Pass name and observed untouched to the pm.Distribution.
        :param covariates_fully_observed:
            A list of columns names, or None.
            The covariates which are considered fully observed
            (cases with missing values will be excluded from the analysis).
        :param strata_columns:
            A list of columns names, or None.
            The data defining strata (one stratum for each unique combination)
        :param model_type:
            Can be one of: piecewise_exponential, weibull
        :param ref_level_dict:
            Optional dictionary of reference levels for categorical covariate.
            Shall be a dict of column name: category level for any covariate for which you dont
            want to use the first level as reference level.
        :return:
            An instance of class BayesianSurvival.Model on which you can call sample()
        """

        with pm.Model(name=self.name_suffix):
            model = self.create_multivariate_model(survival_columns,
                                                   covariates_with_distribution,
                                                   covariates_fully_observed,
                                                   strata_columns,
                                                   model_type,
                                                   ref_level_dict)

        model.build()

        return model

    def create_multivariate_model(self,
                                  survival_columns: Sequence[str],
                                  covariates_with_distribution: Dict[str, Callable] = None,
                                  covariates_fully_observed: Sequence[str] = None,
                                  strata_columns: Sequence[str] = None,
                                  model_type: str = "piecewise_exponential",
                                  ref_level_dict: Dict = None,
                                  pymc_model: pm.Model = None) -> "BayesianSurvival.Model":
        """Includes build_dataset, build_covariate_set and model_object.

        Caller must give a pymc3 model (as parameter or in the context).
        Does everything except for building the model (creating the graph).
        This allows modifications of the covariate set.
        """

        data_set = self.build_dataset(survival_columns,
                                      covariates_fully_observed,
                                      strata_columns)

        pymc_model = pm.modelcontext(pymc_model)
        with pymc_model:
            covariate_set = self.build_covariate_set(data_set,
                                                     covariates_with_distribution,
                                                     covariates_fully_observed,
                                                     ref_level_dict)

            model = self.model_object(data_set,
                                      covariate_set,
                                      model_type)

        return model

    def model_object(self,
                     data_set: DataSet,
                     covariate_set: CovariateSet,
                     model_type: str = "piecewise_exponential",
                     pymc_model: pm.Model = None) -> "BayesianSurvival.Model":
        """Creates an instance of a class implementing model."""

        if model_type == "piecewise_exponential":
            from .exponential import PiecewiseExponentialModel
            model_class = PiecewiseExponentialModel
        elif model_type == "weibull":
            from .weibull import WeibullModel
            model_class = WeibullModel
        else:
            raise ValueError("Unknown model type " + model_type)

        return model_class(data_set=data_set,
                           covariate_set=covariate_set,
                           pymc_model=pymc_model)

    def build_dataset(self,
                      survival_columns: Sequence[str],
                      required_columns: Sequence[str] = None,
                      strata_columns: Sequence[str] = None):
        """Create a DataSet object from the given sources"""
        # Initially, drop NA from those columns which we cannot, need not or want not impute
        if required_columns is None:
            required_columns = []
        if strata_columns is None:
            strata_columns = []

        columns_to_drop_na = [e
                              for l in [survival_columns, required_columns, strata_columns]
                              for e in l]
        current_data = self.data.dropna(axis=0, subset=columns_to_drop_na)
        survival_columns = survival_columns
        # Build survival data
        survival_data = make_survival_dataframe(current_data, survival_columns)
        strata, strata_levels = make_strata(current_data, strata_columns)

        return DataSet(current_data, survival_data, survival_columns, strata, strata_levels)

    @staticmethod
    def default_coefficient(name: str,
                            shape):
        mu_beta = 0
        sd_beta = 10
        return pm.Normal(name=name,
                         mu=mu_beta,
                         sd=sd_beta,
                         shape=shape)

    @staticmethod
    def default_coefficient_for(name: str,
                                ids):
        """Returns default_coefficient for covariates identified by ids"""
        if ids is None:
            return None
        return BayesianSurvival.default_coefficient(name, len(ids))

    def build_covariate_set(self,
                            data_set: DataSet,
                            covariates_with_distribution: Dict[str, Callable] = None,
                            covariates_fully_observed: Sequence[str] = None,
                            ref_level_dict: Dict = None,
                            pymc_model: pm.Model = None):
        """Create a covariate set of covariates using given data, imputation as necessary, and RVs for the coefficients
        """
        pymc_model = pm.modelcontext(pymc_model)

        # Build RVs for covariates and coefficients
        builder = CovariateBuilder(data_set,
                                   covariates_with_distribution,
                                   covariates_fully_observed,
                                   ref_level_dict)

        covariates, covariates_numerical, covariates_boolean, covariates_categorical, covariate_constant = \
            builder.extract_covariates()

        with pymc_model:

            # Prepare priors to numerical covariates

            rvs_numerical, numerical_columns = builder.rvs_ids(covariates_numerical)
            rvs_boolean, boolean_columns = builder.rvs_ids(covariates_boolean)

            # Prepare priors to categorical covariates

            vars_one_hot, categories_one_hot = builder.rvs_ids_categorical(covariates_categorical)

            # Prepare priors for coefficients

            beta_numerical = self.default_coefficient_for('beta numerical', numerical_columns)
            beta_boolean = self.default_coefficient_for('beta boolean', boolean_columns)
            beta_categorical = self.default_coefficient_for('beta categorical', categories_one_hot)

        covariate_groups = [
            CovariateSet.Group(CovariateSet.numerical,
                               rvs_numerical, beta_numerical, numerical_columns),

            CovariateSet.Group(CovariateSet.boolean,
                               rvs_boolean, beta_boolean, boolean_columns),

            CovariateSet.Group(CovariateSet.categorical,
                               vars_one_hot, beta_categorical, categories_one_hot)
        ]

        return CovariateSet(covariate_groups, n_samples=data_set.n_patients)

    class Model(BaseModel):
        def __init__(self,
                     data_set: DataSet,
                     covariate_set: CovariateSet,
                     pymc_model: pm.Model = None):
            super().__init__(pymc_model=pymc_model)
            self.data_set = data_set
            self.covariate_set = covariate_set
            self.result = None

        has_intercept = False

        def vars_of_interest(self) -> List:
            """Returns those RVs that are of interest for storage.

            This will be those RVs that will be used for later analysis or simulation.
            The default implementation returns the covariate coefficients. Extend in subclasses.
            :return: A list of RV objects (not their name)
            """
            return [group.coefficients for group in self.covariate_set.groups if group.ids]

        def sample(self,
                   out_directory: str,
                   n_tune=500,
                   n_burn=500,
                   n_samples=1000,
                   n_jobs=1,
                   vars_to_store=None,
                   **kwargs):
            """Sample from the model

            :param out_directory: Directory used for storage of results and intermediate values
            :param n_tune: Number of samples to tune the sampler
            :param n_burn: Number of samples to discard after tuning
            :param n_samples: Number of samples to use for inference
            :param n_jobs: Number of parallel chains to run
            :param vars_to_store: RVs to store in the trace. Default: None, uses vars_of_interest. Pass "all" if you
             want to store all unobserved variables, or provide a list of RV objects (which should contain at
             least vars_of_interest())
            :param kwargs: Further params passed directly to pymc3's sample
            :return: A Result object
            """

            self.result = BayesianSurvival.Result(out_directory, survival_columns=self.data_set.survival_columns)
            self.result.store_variables(self.data_set, self.covariate_set)

            if vars_to_store is None:
                vars_to_store = self.vars_of_interest()
            if type(vars_to_store) is str and vars_to_store == "all":
                vars_to_store = self.pymc_model.unobserved_RVs

            self.result.sample_from_model(self, n_tune=n_tune,
                                          n_burn=n_burn,
                                          n_samples=n_samples,
                                          n_jobs=n_jobs,
                                          vars_to_store=vars_to_store,
                                          **kwargs)

            return self.result

        def build(self):
            raise NotImplementedError()

        def prepare_new_data(self,
                             data: pd.DataFrame):
            # Must prepare data as we did with training data
            new_strata = strata(data, self.data_set.strata_levels)
            new_covariate_data = self.covariate_set.extract_covariate_data(data)

            if np.any(new_strata.isnull()):
                raise ValueError("The data for prediction contains cases assigned to strata that were not trained")
            for type_, df in new_covariate_data:
                if df is not None and np.any(df.isnull()):
                    raise ValueError("Missing values in data for prediction (category: {})."
                                     " Imputation is not implemented.".format(type_))

            return new_strata, new_covariate_data

        def survival_function(self,
                              result: "BayesianSurvival.Result",
                              data: pd.DataFrame,
                              alpha: float=0.05) -> pd.DataFrame:
            """Returns the empirical survival function predicted from new covariate data

            :param result:
                A result returned from sample() of this model (can be retrieved from disk)
            :param data:
                A data frame of independent covariate data. Must have columns for all covariates an strata.
            :param alpha:
                The alpha level for generating posterior intervals.
            :return:
                A data frame with columns patient, x, mean, median, hpd_lower, hpd_upper.
                Regarding the latter four as y, the interval from x[i] to x[i+1] has level y[i+1]
                (like where="pre" in matplotlib.pyplot.step).
                The y value at x=0 will be 1.0.
            """
            return self.empirical_function("survival", result, data, alpha)

        def cumulative_hazard_function(self,
                                       result: "BayesianSurvival.Result",
                                       data: pd.DataFrame,
                                       alpha: float=0.05) -> pd.DataFrame:
            """Returns the empirical cumulative hazard function predicted from new covariate data

            :param result:
                A result returned from sample() of this model (can be retrieved from disk)
            :param data:
                A data frame of independent covariate data. Must have columns for all covariates an strata.
            :param alpha:
                The alpha level for generating posterior intervals.
            :return:
                A data frame with columns patient, x, mean, median, hpd_lower, hpd_upper.
                Regarding the latter four as y, the interval from x[i] to x[i+1] has level y[i+1]
                (like where="pre" in matplotlib.pyplot.step).
                The y value at x=0 will be 0.
            """
            return self.empirical_function("cumulative hazard", result, data, alpha)

        def empirical_function(self,
                               function_type: str,
                               result: "BayesianSurvival.Result",
                               data: pd.DataFrame,
                               alpha: float = 0.05) -> pd.DataFrame:
            raise NotImplementedError()

        def sample_ppc(self,
                       result: "BayesianSurvival.Result",
                       data: pd.DataFrame,
                       n_samples=500,
                       draws_per_sample=1,
                       seed=None) -> pd.DataFrame:
            """Make survival time prediction for new covariate data.

             Samples from the posterior predictive distribution of the given result.

            :param result:
                A result returned from sample() of this model (can be retrieved from disk)
            :param data:
                A data frame of independent covariate data. Must have columns for all covariates an strata.
            :param n_samples:
                Number of posterior predictive samples to generate.
            :param draws_per_sample:
                The number of random draws from the distribution
                specified by the parameters in each sample of the trace.
            :param seed:
                Random seed
            :return:
                A data frame with three columns:
                    patient: patient id, the index value if the corresponding row in the data
                    time: predicted survival time
                    status: whether the predicted time is death (1) or censored (0)
                Thus, the shape of the data frame is (data.shape[0]*draws*size, 3)
            """
            raise NotImplementedError()

    class Result(Result):
        """Captures the result (trace) of a model sampling operation"""

        def __init__(self,
                     out_directory: str,
                     endpoint: str = None,
                     survival_columns: Sequence[str] = None):
            if endpoint is None:
                if survival_columns is None:
                    raise ValueError("Need either plain endpoint or survival columns with endpoint at [0]")
                endpoint = survival_columns[0]

            super().__init__(out_directory, result_id=endpoint)
            self.variable_df_file = self.out_directory + "/variables-" + self.result_id + ".feather"

        def store_variables(self,
                            data_set: DataSet,
                            covariate_set: CovariateSet):

            covariate_set.summary_df(data_set).pipe(feather.write_dataframe, self.variable_df_file)

        def analyse(self) -> pd.DataFrame:
            """Creates a summary data frame with HR estimates and confidence measures.

            The data frame is written as a Feather file to disk and returned"""
            variables_df = feather.read_dataframe(self.variable_df_file)
            self.get_pymc_model()
            self.get_trace()

            varnames = list(variables_df["variable name"].drop_duplicates())

            summary = pd.concat([
                variables_df.set_index("formatted variable name"),
                df_summary_pereira(self.trace,
                                   varnames=varnames,
                                   start=self.n_burn,
                                   constants={"endpoint": self.result_id})
            ], axis=1)  # type: pd.DataFrame

            feather.write_dataframe(summary, self.out_directory + "/coefficients-" + self.result_id + ".feather")

            return summary

            # pm.plot_posterior(trace, varnames=varnames_of_interest)
            # plt.savefig(out_directory + "/posteriors.pdf")
            # plt.close()

            # pm.traceplot(trace, varnames=vars_of_interest)
            # plt.savefig(out_directory + "/traces.pdf")
            # plt.close()

    @staticmethod
    def sample(out_directory: str,
               models: Sequence[Model],
               n_tune=25000,
               n_burn=25000,
               n_samples=50000,
               seed=-1,
               n_jobs=1):
        def results():
            for model in models:
                endpoint_dir = out_directory + "/" + model.data_set.survival_columns[0]
                yield model.sample(endpoint_dir,
                                   n_tune,
                                   n_burn,
                                   n_samples,
                                   seed,
                                   n_jobs
                                   )
        return list(results())

    @staticmethod
    def analyse(results: Sequence[Result],
                out_directory=None):
        if not out_directory:
            out_directory = results[0].out_directory

        pd.concat([
            result.analyse() for result in results
        ], axis=0).pipe(
            feather.write_dataframe,
            out_directory + "/hazard_ratios.feather"
        )

    @staticmethod
    def result_from_disk(out_directory: str,
                         endpoint: str = None,
                         survival_columns: Sequence[str] = None):
        return BayesianSurvival.Result(out_directory,
                                       endpoint=endpoint,
                                       survival_columns=survival_columns)
