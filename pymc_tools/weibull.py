import numpy as np
import pandas as pd
import pymc3 as pm
from theano import tensor as T

from .survival import BayesianSurvival


class WeibullModel(BayesianSurvival.Model):
    """Accelerated failure time model using a Weibull baseline survival function

    http://austinrochford.com/posts/2017-10-02-bayes-param-survival.html
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.log_mean = None
        self.log_std = None

    has_intercept = True

    @staticmethod
    def _gumbel_survival(x, mu, beta):
        # http://www.itl.nist.gov/div898/handbook/eda/section3/eda366g.htm
        x = (x - mu) / beta
        return 1. - T.exp(- T.exp(-x))

    @staticmethod
    def _gumbel_cumulative_hazard(x, mu, beta):
        return -T.log(WeibullModel._gumbel_survival(x, mu, beta))

    @staticmethod
    def _gumbel_survival_outer(x, mu, beta):
        """Returns an array of gumbel_survivals

        Uses all combinations of x and (mu, beta), that means, mu and better are only used in parallel.
        Returned shape is (len(x), len(mu)) where len(beta)==len(mu)
        """
        x = np.subtract.outer(x, mu) / beta
        return 1. - np.exp(- np.exp(-x))

    @staticmethod
    def _gumbel_cumulative_hazard_outer(x, mu, beta):
        return -np.log(WeibullModel._gumbel_survival_outer(x, mu, beta))

    def log_survival(self, x):
        return (np.log(x) - self.log_mean) / self.log_std

    def invert_log_survival(self, x):
        return np.exp(self.log_mean + self.log_std * x)

    def build(self):
        with self.pymc_model:
            constants_sd = 5.
            intercept_per_strata = pm.Normal(name="intercept", mu=0, sd=constants_sd,
                                             shape=len(self.data_set.strata_levels))
            intercepts_per_patient = intercept_per_strata[self.data_set.strata.values]

            gumbel_beta_per_strata = pm.HalfNormal(name="gumbel beta", sd=constants_sd,
                                                   shape=len(self.data_set.strata_levels))
            gumbel_beta = gumbel_beta_per_strata[self.data_set.strata.values]

            # shape: (n_patients,)
            hazard = self.covariate_set.covariates_dot_coefficients(mode="theano")
            hazard = hazard + intercepts_per_patient

            log_survival = np.log(self.data_set.survival_data["time"])
            self.log_mean = log_survival.mean()
            self.log_std = log_survival.std()
            logstd_survival = (log_survival - self.log_mean) / self.log_std
            logstd_survival = logstd_survival.values

            censored_patient_mask = self.data_set.survival_data["status"] == 0
            observed_patient_mask = ~censored_patient_mask
            censored_patient_idc = np.nonzero(censored_patient_mask)[0]
            observed_patient_idc = np.nonzero(observed_patient_mask)[0]

            pm.Gumbel(name="y_obs",
                      mu=hazard[observed_patient_idc],
                      beta=gumbel_beta[observed_patient_idc],
                      observed=logstd_survival[observed_patient_mask])

            pm.Bernoulli(name='y_cens',
                         p=WeibullModel._gumbel_survival(
                             logstd_survival[censored_patient_mask],
                             mu=hazard[censored_patient_idc],
                             beta=gumbel_beta[censored_patient_idc]),
                         observed=np.ones(censored_patient_mask.sum())
                         )

            # TODO: implement strata by using one intercept and one gumbel_beta per-stratum

    def vars_of_interest(self):
        return super().vars_of_interest() + [self.pymc_model.named_vars["gumbel beta"]]

    def empirical_function(self,
                           function_type: str,
                           result: BayesianSurvival.Result,
                           data: pd.DataFrame,
                           alpha: float = 0.05) -> pd.DataFrame:
        new_strata, new_covariate_data = self.prepare_new_data(data)

        max_not_censored = self.data_set.survival_data.loc[self.data_set.survival_data["status"] == 1, "time"].max()
        x = np.concatenate([
            np.arange(0, max_not_censored),
            np.linspace(max_not_censored, self.data_set.survival_data["time"].max())
        ])
        log_x = self.log_survival(x)
        new_n_patients = len(data)

        trace = result.get_trace()

        # shape: (n_samples, 1) -> (n_samples,)
        gumbel_pred = np.squeeze(trace["gumbel beta"])
        # shape: new_n_patients x n_samples
        hazard_pred = self.covariate_set.covariates_dot_coefficients(covariate_data=new_covariate_data,
                                                                     coefficients=trace)

        # shape (new_n_patients,)
        mean_hazard_pred = np.mean(hazard_pred, axis=1)
        mean_gumbel_pred = np.mean(gumbel_pred)

        if function_type == "survival":
            func = WeibullModel._gumbel_survival_outer
        elif function_type == "cumulative hazard":
            func = WeibullModel._gumbel_cumulative_hazard_outer
        else:
            raise ValueError("Unknown function type " + function_type)

        percentiles = 100 * np.array([alpha / 2., 0.5, 1. - alpha / 2.])
        function_df = pd.DataFrame()
        for patient_idx in range(new_n_patients):
            # shape (len(x),)
            mean_func_result = func(log_x,
                                    mu=mean_hazard_pred[patient_idx],
                                    beta=mean_gumbel_pred)
            # shape (len(x), n_samples)
            func_result = func(log_x,
                               mu=hazard_pred[patient_idx],
                               beta=gumbel_pred)
            hpd = np.percentile(func_result, percentiles, axis=1)
            # split at first dim (one python variable for each percentile)
            hpd_lower, median, hpd_upper = hpd[0], hpd[1], hpd[2]

            function_df = function_df.append(pd.DataFrame({
                "patient": data.index[patient_idx],
                "x": x,
                "mean": mean_func_result,
                "median": median,
                "hpd_lower": hpd_lower,
                "hpd_upper": hpd_upper
            }))

        return function_df
