from typing import Sequence

import pymc3 as pm

from .basemodel import BaseModel
from .result import Result


class SamplingBaseModel(BaseModel):

    def __init__(self,
                 pymc_model: pm.Model=None,
                 **kwargs):
        super().__init__(pymc_model=pymc_model,
                         **kwargs)

    def sample_to_result(self,
                         out_directory: str,
                         result_id: str,
                         n_tune=500,
                         n_burn=500,
                         n_samples=1000,
                         n_jobs=1,
                         vars_to_store=None,
                         varnames_to_store: Sequence[str]=None,
                         **kwargs) -> Result:
        result = Result(out_directory=out_directory,
                        result_id=result_id)
        result.sample_from_model(self,
                                 n_tune=n_tune,
                                 n_burn=n_burn,
                                 n_samples=n_samples,
                                 n_jobs=n_jobs,
                                 vars_to_store=vars_to_store,
                                 varnames_to_store=varnames_to_store,
                                 **kwargs)
        return result

class Model(SamplingBaseModel):
    """
    Convenience class around pm.Model/BaseModel and result.

    This class is not intended to be a parent class:
    If you want to provide custom result subclasses
    simply inherit directly from BaseModel.
    If you want to overload sample(), inherit from SamplingBaseModel.
    """

    def __init__(self,
                 pymc_model: pm.Model=None,
                 **kwargs):
        super().__init__(pymc_model=pymc_model,
                         **kwargs)

    def sample(self,
               out_directory: str,
               result_id: str,
               n_tune=500,
               n_burn=500,
               n_samples=1000,
               n_jobs=1,
               vars_to_store=None,
               varnames_to_store: Sequence[str]=None,
               **kwargs) -> Result:
        return self.sample_to_result(out_directory=out_directory,
                                     result_id=result_id,
                                     n_tune=n_tune,
                                     n_burn=n_burn,
                                     n_samples=n_samples,
                                     n_jobs=n_jobs,
                                     vars_to_store=vars_to_store,
                                     varnames_to_store=varnames_to_store,
                                     **kwargs)
