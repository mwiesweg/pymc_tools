from typing import Sequence

import pandas as pd
import numpy as np
import feather

import pymc3 as pm

from .. import BayesianSurvival, CovariateSet, CovariateBuilder, Model, Result


class MergedEndpointParameters:

    def __init__(self,
                 out_directory: str,
                 data: pd.DataFrame,
                 endpoints: Sequence[Sequence[str]],
                 covariate: str,
                 ref_level: str):
        """        
        :param out_directory: Where to write output files 
        :param data: A data frame containing survival data
        :param endpoints: Sequence of pairs time, status of column names for survival endpoints
        :param covariate: Column name of covariate
                          (this example performs only univariate analysis of a categorical covariate)
        :param ref_level: Reference level for categorical covariate
        """
        self.out_directory = out_directory
        self.data = data
        self.endpoints = endpoints
        self.covariate = covariate
        self.ref_level = ref_level

        self.cats = data[covariate].cat.categories
        self.ref_level = ref_level


class SubModel:

    def __init__(self,
                 params: MergedEndpointParameters,
                 entity: str,
                 endpoint: Sequence[str],
                 surv: BayesianSurvival,
                 shared_beta=None):

        self.data_set = surv.build_dataset(endpoint, [params.covariate])
        mutation_data = self.data_set.data[params.covariate].copy()
        mutation_data.cat.remove_unused_categories(inplace=True)

        with pm.Model("{}_{}".format(entity, endpoint[0])):

            builder = CovariateBuilder(self.data_set,
                                       covariates_fully_observed=[params.covariate],
                                       ref_level_dict={params.covariate: params.ref_level})
            rvs, ids = builder.rvs_ids_categorical(mutation_data)
            # ids is a list of tuples (covariate, cat) where cat is a value from cats, excluding ref_level
            self.beta_subset_idc = np.asarray([params.cats.get_loc(cat) for _, cat in ids])

            if shared_beta is None:
                # we are an independent model
                shared_beta = surv.default_coefficient("ras beta", len(ids))
            else:
                # We share parent's shared_beta, possibly with coefficients for mutations that are missing in our data.
                # Create a subset of coefficients
                print("Setting up a submodel for {}, {} with {} patients and {} different mutations".format(
                    entity, endpoint[0], self.data_set.n_patients, mutation_data.cat.categories.size))
                shared_beta = shared_beta[self.beta_subset_idc]

            group = CovariateSet.Group(CovariateSet.categorical,
                                       rvs,
                                       shared_beta,
                                       ids)
            covariate_set = CovariateSet([group],
                                         self.data_set.n_patients)
            self.model = surv.model_object(self.data_set, covariate_set)
            self.model.build()

    def name_for(self,
                 name: str):
        return self.model.pymc_model.name_for(name)

    def extract_start_values(self,
                             trace,
                             previous_model: "SubModel",
                             beta_separately: bool):
        """Extract start values for this model from the trace of a similar model.

        To be called with the trace of an independent submodel only (=previous model)."""
        if trace is None:
            return None

        name_for_shared_beta = self.name_for("ras beta")

        relevant_varnames = self.model.relevant_varnames()
        if name_for_shared_beta not in relevant_varnames:
            relevant_varnames += [name_for_shared_beta]

        start_values = self.model.extract_start_values_for(trace, relevant_varnames)
        print(trace.varnames)
        print(start_values.keys())
        prev_beta_values = start_values.pop(name_for_shared_beta)

        def shared_beta_start_values():
            for idx in self.beta_subset_idc:
                prev_index = np.nonzero(previous_model.beta_subset_idc == idx)[0]
                if len(prev_index):
                    yield prev_beta_values[prev_index[0]]
                else:
                    yield 0.

        beta_values = np.asarray(list(shared_beta_start_values()))
        if beta_separately:
            return start_values, beta_values
        else:
            start_values[name_for_shared_beta] = beta_values
            return start_values

    def generate_init_trace(self,
                            previous_trace=None,
                            previous_model: "SubModel" = None):
        """Generate init trace from this model, possibly starting from a trace of a similar model.

        To be called on an independent submodel only."""

        start_values = self.extract_start_values(previous_trace, previous_model, beta_separately=False)
        print("Start values for init trace:")
        print(start_values)

        self.model.diagnose_specification(start_values)
        return self.model.plain_sample(start=start_values,
                                       random_seed=1)


class MetaModel(Model):

    def __init__(self,
                 params: MergedEndpointParameters,
                 entity: str):
        super().__init__(pm.Model("meta {}".format(entity)))
        self.params = params
        self.entity = entity
        self.submodels = []
        self.independent_models_lists = []

        df = params.data.query("entity == @entity")
        surv = BayesianSurvival(df)

        with self:
            self.shared_beta = surv.default_coefficient("ras beta", params.cats.size)

        for endpoint in params.endpoints:
            with self:
                self.submodels.append(SubModel(params, entity, endpoint, surv, self.shared_beta))

            independent_models = []
            if entity == "CRC":
                fracs = np.arange(0.3, 1, 0.05)
            else:
                fracs = [0.3, 0.35, 0.4, 0.5, 0.8]
            for frac in fracs:
                surv_short = BayesianSurvival(df.sample(frac=frac, random_state=1))
                independent_models.append(SubModel(params, entity, endpoint, surv_short))
            independent_models.append(SubModel(params, entity, endpoint, surv))

            self.independent_models_lists.append(independent_models)

    def generate_start_values(self):
        meta_start_values = {}
        ras_start_values_df = pd.DataFrame(np.NaN, index=self.params.cats, columns=np.arange(len(self.submodels)))
        print("Generating start values for {}".format(self.pymc_model.name))

        for i, (ind_models, submodel) in enumerate(zip(self.independent_models_lists, self.submodels)):
            trace = None
            previous_model = None
            for ind_model in ind_models:
                print("independent model with {} patients".format(ind_model.data_set.n_patients))
                trace = ind_model.generate_init_trace(previous_trace=trace,
                                                      previous_model=previous_model)
                previous_model = ind_model

            start_values, beta_values = submodel.extract_start_values(trace,
                                                                      previous_model=previous_model,
                                                                      beta_separately=True)

            meta_start_values.update(start_values)

            ras_start_values = pd.Series(beta_values, index=self.params.cats[submodel.beta_subset_idc])
            ras_start_values_df[i] = ras_start_values  # assignment by index (categories)

        meta_ras_start_values = ras_start_values_df.mean(axis=1).fillna(0)
        meta_start_values[self.pymc_model.name_for("ras beta")] = meta_ras_start_values

        return meta_start_values

    n_tune = 25000
    n_burn = 25000
    n_samples = 50000

    @staticmethod
    def directory(out_directory, entity):
        return "{}/{}".format(out_directory, entity)

    @staticmethod
    def summary_filename(result):
        return result.format_filename("{}/summary-{}.feather")

    def metasample(self):
        print("Generating start values for {}".format(self.entity))
        start_values = self.generate_start_values()
        print("Overall start_values:")
        print(start_values)

        result = Result(out_directory=self.directory(self.params.out_directory, self.entity),
                        result_id=self.entity)
        self.diagnose_specification(start_values)
        result.sample_from_model(self,
                                 n_tune=self.n_tune,
                                 n_burn=self.n_burn,
                                 n_samples=self.n_samples,
                                 n_jobs=10,
                                 vars_to_store=[self.shared_beta],
                                 random_seed=1,
                                 start=start_values)
        summary = result.df_summary_pereira(variable_name_as_column="variable id")
        summary["mutation"] = self.params.cats
        summary["entity"] = self.entity
        print(summary)
        feather.write_dataframe(summary, self.summary_filename(result))

        return result

    @staticmethod
    def from_disk(out_directory, entity: str):
        result = Result.load(out_directory=MetaModel.directory(out_directory, entity),
                             result_id=entity,
                             n_discard=MetaModel.n_tune + MetaModel.n_burn)
        summary = feather.read_dataframe(MetaModel.summary_filename(result))
        return result, summary


def sample(params: MergedEndpointParameters,
           entities: Sequence[str]):
    for entity in entities:
        meta_model = MetaModel(params, entity)
        meta_model.metasample()

    
