
import numpy as np
import theano.tensor as tt
from scipy import stats

import pymc3 as pm
from pymc3.math import logit, invlogit
from pymc3.distributions.dist_math import bound
from pymc3.distributions import draw_values, generate_samples
from pymc3.distributions.continuous import UnitContinuous, assert_negative_support, get_tau_sd


class LogitNormal(UnitContinuous):
    R"""
    Logit-Normal log-likelihood.
    .. math::
       f(x \mid \mu, \tau) =
           \frac{1}{x(1-x)} \sqrt{\frac{\tau}{2\pi}}
           \exp\left\{ -\frac{\tau}{2} (logit(x)-\mu)^2 \right\}
    ========  =========================================================================
    Support   :math:`x \in (0, 1)`
    Mean      no analytical solution
    Variance  no analytical solution
    ========  =========================================================================

    .. plot::

        import matplotlib.pyplot as plt
        import numpy as np
        import scipy.stats as st
        from scipy.special import logit
        x = np.linspace(0, 1, 1000)
        fig, ax = plt.subplots()
        f = lambda mu, sd : st.norm.pdf(logit(x), loc=mu, scale=sd) * 1/(x * (1-x))
        plot_pdf = lambda a, b : ax.plot(x, f(a,b), label=r'$\mu$={0}, $\sigma$={1}'.format(a,b))
        plot_pdf(0, 0.3)
        plot_pdf(0, 1)
        plot_pdf(0, 2)
        plot_pdf(1, 1)
        plt.legend(loc='upper right', frameon=False)
        ax.set(xlim=[0, 1], ylim=[0, 6], xlabel='x', ylabel='f(x)')
        plt.show()

    Parameters
    ----------
    mu : float
        Location parameter.
    sd : float
        Scale parameter (sd > 0).
    tau : float
        Scale parameter (tau > 0).
    """

    def __init__(self, mu=0, sd=None, tau=None, **kwargs):
        self.mu = mu = tt.as_tensor_variable(mu)
        tau, sd = get_tau_sd(tau=tau, sd=sd)
        self.sd = tt.as_tensor_variable(sd)
        self.tau = tau = tt.as_tensor_variable(tau)

        self.median = pm.math.sigmoid(mu)
        assert_negative_support(sd, 'sd', 'LogitNormal')
        assert_negative_support(tau, 'tau', 'LogitNormal')

        super(LogitNormal, self).__init__(**kwargs)

    def random(self, point=None, size=None, repeat=None):
        mu, _, sd = draw_values([self.mu, self.tau, self.sd],
                                point=point)
        return invlogit(generate_samples(stats.norm.rvs, loc=mu, scale=sd,
                                         dist_shape=self.shape,
                                         size=size))

    def logp(self, value):
        mu = self.mu
        tau = self.tau
        return bound(-0.5 * tau * (logit(value) - mu) ** 2
                     + 0.5 * tt.log(tau / (2. * np.pi))
                     - tt.log(value * (1 - value)), value > 0, value < 1, tau > 0)
