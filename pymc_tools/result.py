from typing import Sequence
from warnings import warn
import os
import pickle
import copy
import json

import pandas as pd
import pymc3 as pm
import pymc3.backends
import pymc3.backends.base
import theano.tensor as tt

from .utilities import prepare_directory
from .basemodel import BaseModel
from .analysis import df_summary, df_summary_pereira


class Result:
    """Generic wrapper around a trace with on-disk storage convenience"""

    def __init__(self,
                 out_directory: str,
                 result_id: str):

        self.out_directory = prepare_directory(out_directory)
        self.result_id = result_id
        self.chain_directory = prepare_directory(self.out_directory + "/chains-" + self.result_id)
        self.model_file = self.out_directory + "/model-" + self.result_id
        self.result_parameter_file = self.out_directory + "/result-params-" + self.result_id + ".json"

        self.n_burn = 0
        self.pymc_model = None
        self.trace = None
        self.stored_varnames = None

    @classmethod
    def load(cls,
             out_directory: str,
             result_id: str,
             **kwargs):
        result = cls(out_directory=out_directory,
                     result_id=result_id,
                     **kwargs)
        result.load_result_parameters()
        result.load_pymc_model()
        result.load_trace()
        return result

    def format_filename(self,
                        template: str):
        """Formats the given template string with (first) out_directory and (second) result_id"""
        return template.format(self.out_directory, self.result_id)

    def store_model(self, model: pm.Model):
        pickle.dump(model, open(self.model_file, "wb"))
        self.pymc_model = model

    def store_result_parameters(self):
        with open(self.result_parameter_file, "w") as file:
            json.dump({
                "stored_varnames": self.stored_varnames,
                "n_burn": self.n_burn
            }, file)

    def create_backend(self,
                       vars_):
        # must delete files else backend wants to append
        if os.path.exists(self.chain_directory):
            for file in os.listdir(self.chain_directory):
                result_file = self.chain_directory + "/" + file
                print("Overwriting result file " + result_file)
                os.unlink(result_file)

        return pm.backends.Text(self.chain_directory, vars=vars_)

    def store_result(self,
                     trace: pm.backends.base.MultiTrace,
                     n_burn: int):
        self.n_burn = n_burn
        self.trace = trace

    def load_pymc_model(self):
        if not self.pymc_model:
            self.pymc_model = pickle.load(open(self.model_file, "rb"))

    def load_result_parameters(self):
        with open(self.result_parameter_file, "r") as file:
            params = json.load(file)
            self.n_burn = params["n_burn"]
            self.stored_varnames = params["stored_varnames"]

    def cloned_model(self):
        self.load_pymc_model()
        return copy.deepcopy(self.pymc_model)

    def load_trace(self):
        if not self.trace:
            print("Loading trace for " + self.result_id)
            self.trace = pm.backends.text.load(self.chain_directory, model=self.pymc_model)
            print("Loaded trace.")

    def get_trace(self,
                  varname: str = None,
                  thin: int = 1):
        self.load_trace()

        print(self.trace.varnames)

        if varname:
            varname = self.pymc_model.name_for(varname)
            return self.trace.get_values(varname, burn=self.n_burn, thin=thin)
        else:
            return self.trace[self.n_burn::thin]

    def sample_from_model(self,
                          model: BaseModel,
                          n_tune=500,
                          n_burn=500,
                          n_samples=1000,
                          n_jobs=1,
                          vars_to_store=None,
                          varnames_to_store: Sequence[str]=None,
                          **kwargs):
        """Sample from the model

        Note: In subclasses, will be called from the Model subclass which constructs a correctly typed
        Result subclass in a sample() method. Placing the code in Result allows a generic implementation.

        :param model: Model object to sample from
        :param n_tune: Number of samples to tune the sampler
        :param n_burn: Number of samples to discard after tuning
        :param n_samples: Number of samples to use for inference
        :param n_jobs: Number of parallel chains to run
        :param vars_to_store: RVs to store in the trace. Default: None, uses all unobserved RVs.
        :param varnames_to_store: Names of RVs to store in the trace. Interpreted if vars_to_store is None.
        :param kwargs: Further params passed directly to pymc3's sample
        :return:
        """

        if vars_to_store is None:
            if varnames_to_store is not None:
                if isinstance(varnames_to_store, str):
                    varnames_to_store = [varnames_to_store]
                vars_to_store = [model.pymc_model[name] for name in varnames_to_store]
            else:
                vars_to_store = model.pymc_model.unobserved_RVs
        else:
            if varnames_to_store is not None:
                warn("Both vars_to_store and varnames_to_store given. Ignoring varnames_to_store.")
            if isinstance(vars_to_store, tt.Variable):
                vars_to_store = [vars_to_store]

        self.store_model(model.pymc_model)
        self.stored_varnames = [rv.name for rv in vars_to_store]

        with model:
            backend = self.create_backend(vars_to_store)
            trace = model.plain_sample(n_tune=n_tune,
                                       n_burn=n_burn,
                                       n_samples=n_samples,
                                       n_jobs=n_jobs,
                                       backend=backend,
                                       **kwargs)
            self.store_result(trace, n_burn)

        # call last, after setting stored_varnames and n_burn
        self.store_result_parameters()

    def df_summary(self,
                   variable_name_as_column: str = None) -> pd.DataFrame:
        self.load_trace()
        return df_summary(self.trace,
                          varnames=self.stored_varnames,
                          constants={"id": self.result_id},
                          variable_name_as_column=variable_name_as_column)

    def df_summary_pereira(self,
                           theta_null: float = 0,
                           variable_name_as_column: str = None) -> pd.DataFrame:
        self.load_trace()
        return df_summary_pereira(self.trace,
                                  varnames=self.stored_varnames,
                                  constants={"id": self.result_id},
                                  theta_null=theta_null,
                                  variable_name_as_column=variable_name_as_column)

    def store_formatted_summary(self):
        self.load_trace()
        pm.summary(self.trace,
                   varnames=self.stored_varnames,
                   to_file=self.format_filename("{}/summary-{}"))

