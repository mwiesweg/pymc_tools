from typing import Dict

import pandas as pd
import numpy as np

import pymc3 as pm

from .utilities import pereira_sternberg


def df_summary(trace,
               varnames=None,
               constants: Dict = None,
               start: int = 0,
               variable_name_as_column: str = None) -> pd.DataFrame:
    """Returns a summary data frame like pymc3.stats.df_summary

    In addition, this version will include the median.
    Variable name is used as the index. If variable_name_as_column is not None, stores
    the variable names as a column named like this and resets the index."""
    summary = pd.concat([
        pm.stats.df_summary(trace, varnames=varnames, start=start),
        pm.stats.df_summary(trace, varnames=varnames, start=start,
                            stat_funcs=[lambda x:
                                        pd.Series(np.median(x, axis=0), name='median')
                                        ]
                            )
    ], axis=1)  # type: pd.DataFrame

    for key, value in constants.items():
        summary[key] = value

    if variable_name_as_column is not None:
        summary = summary.reset_index().rename(columns={"index": variable_name_as_column})

    return summary


def df_summary_pereira(trace,
                       varnames=None,
                       start: int = 0,
                       constants: Dict = None,
                       theta_null: float = 0,
                       variable_name_as_column: str = None
                       ) -> pd.DataFrame:
    """Returns a summary data frame like pymc3.stats.df_summary

    In addition, this version will include the median and
    the Pereira-Sternberg p-value-like measure of confidence."""

    # TODO: allow theta_null to be a dict, and build p* series row-wise by slicing the trace on varnames.

    summary = pd.concat([
        df_summary(trace, varnames, constants, start, variable_name_as_column=None),
        pm.stats.df_summary(trace, varnames=varnames, start=start,
                            stat_funcs=[lambda x:
                                        pd.Series(pereira_sternberg(x, theta_null=theta_null, axis=0), name='p')
                                        ]
                            )
    ], axis=1)  # type: pd.DataFrame

    if variable_name_as_column is not None:
        summary = summary.reset_index().rename(columns={"index": variable_name_as_column})

    return summary
