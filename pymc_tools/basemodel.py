from typing import Sequence

import pymc3 as pm

from .utilities import diagnose_model_specification


class BaseModel:
    """Generic wrapper for pm.Model with some convenience functions"""

    def __init__(self,
                 pymc_model: pm.Model=None,
                 **kwargs):
        if pymc_model is not None:
            self.pymc_model = pymc_model
        elif pm.Model.get_contexts():
            self.pymc_model = pm.modelcontext(pymc_model)
        else:
            self.pymc_model = pm.Model(**kwargs)
        self.result = None

    def __enter__(self):
        self.pymc_model.__enter__()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.pymc_model.__exit__(exc_type, exc_val, exc_tb)

    def relevant_varnames(self, with_prefix: bool = True):
        def relevant_varnames_generator():
            for rv in self.pymc_model.vars:
                name = rv.name
                if name.endswith("_missing"):
                    continue
                if with_prefix:
                    yield name
                else:
                    yield self.pymc_model.name_of(rv.name)

        return list(relevant_varnames_generator())

    def extract_start_values(self,
                             trace,
                             with_prefix: bool = True):
        """From a trace sampled by a model of the same class, extract a dict suitable as sample start parameter.

        Note: The trace can originate from model run on data subsets.
        In this case, subclasses should take appropriate action (probably subclass transform_start_value)
        Note: If with_prefix, the dictionary keys will be variable names with prefix, else without.
        """
        return self.extract_start_values_for(trace, self.relevant_varnames(with_prefix=False), with_prefix)

    def extract_start_values_for(self,
                                 trace,
                                 varnames: Sequence[str],
                                 with_prefix: bool = True):
        """From a trace sampled by a model of the same class, extract a dict of given variables
        as sample start parameter.

        Varnames can given be with or without prefix.
        Only variables found in the trace will be returned. No error is raised if a variable is not found.
        """
        def valid_tracepoints():
            for name in varnames:
                basename = self.pymc_model.name_of(name)
                prefixed_name = self.pymc_model.name_for(name)
                if basename in trace.varnames:
                    name = basename
                elif prefixed_name in trace.varnames:
                    name = prefixed_name
                else:
                    continue
                last_point = trace[name][-1]
                last_point = self.transform_start_value(basename, prefixed_name, last_point)
                if last_point.shape[0]:
                    yield prefixed_name if with_prefix else basename, last_point

        return dict(valid_tracepoints())

    def transform_start_value(self,
                              basename,
                              prefixed_name,
                              value):
        """Called from extra_start_values methods. Allows subclasses to take transform actions on certain values."""
        return value

    def diagnose_specification(self,
                               start=None):
        diagnose_model_specification(self.pymc_model,
                                     start=start)

    def plain_sample(self,
                     n_tune=500,
                     n_burn=500,
                     n_samples=1000,
                     n_jobs=1,
                     backend=None,
                     **kwargs):
        """Simply calls pm.sample. Edits some parameters as appropriate,
         and uses for some parameters a naming scheme consistent within pymc_tools.
        """
        if "start" in kwargs:
            value = kwargs["start"]
            # add model prefix if not yet done
            if value is not None:
                kwargs["start"] = {self.pymc_model.name_for(name): data for name, data in value.items()}
        if backend is not None:
            if "trace" in kwargs:
                raise ValueError("Received both a backend and a trace parameter: Can only use one.")
            kwargs["trace"] = backend

        with self.pymc_model:
            return pm.sample(draws=n_burn + n_samples,
                             tune=n_tune,
                             njobs=n_jobs,
                             **kwargs)
