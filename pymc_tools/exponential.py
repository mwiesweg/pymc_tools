import copy

import numpy as np
import pandas as pd
import pymc3 as pm
import scipy.stats as stats
from scipy.ndimage.interpolation import zoom
from theano import tensor as T

from pymc_tools.survival import BayesianSurvival
from .utilities import pick_rows, reduce_intervals


class PiecewiseExponentialModel(BayesianSurvival.Model):
    """Semiparametric proportional hazards model

    Based on a piece-wise exponential model with a piece-wise constant baseline hazard.
    Calculations are performed using an equivalent Poisson regression model.
    http://austinrochford.com/posts/2015-10-05-bayes-survival.html
    http://docs.pymc.io/notebooks/survival_analysis.html
    http://data.princeton.edu/wws509/notes/c7.pdf
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.interval_lengths = None
        self.interval_bounds = None

    has_intercept = False

    def build(self):
        fixed_length_intervals = False
        self.interval_lengths, self.interval_bounds, death, exposure = (
            self._calculate_intervals_fixed_length() if fixed_length_intervals
            else self._calculate_intervals_variable_length()
        )

        n_intervals = len(self.interval_lengths)

        with self.pymc_model:
            # Cox model:
            # λ(t)=λ0(t)exp(xβ)

            # Priors:
            # baseline hazard as a piecewise function per-interval (but not per-patient)
            # λ0(t)=λj if sj≤t<sj+1 with λj∼Gamma(10−2,10−2)
            # we need one baseline hazard per-stratum. Create (n_strata x n_intervals) matrix
            lambda0_per_strata = pm.Gamma(name='lambda0 per strata', alpha=0.01, beta=0.01,
                                          shape=(len(self.data_set.strata_levels), n_intervals))
            # create a matrix n_patients X n_intervals with per-interval hazard per-patient
            # from lambda0_per_strata, create a matrix with one row per-patient
            # indexed from lambda0_per_strata by strata
            lambda0_per_patient = pick_rows(lambda0_per_strata, self.data_set.strata, second_dim=n_intervals)

            # this is exp(xβ)
            attr_hazard = T.exp(self.covariate_set.covariates_dot_coefficients(mode="theano"))
            # attr_hazard is shaped (n_patients,) - make column vector for element-wise multiplication
            attr_hazard = attr_hazard.reshape((self.data_set.n_patients, 1))

            # so λ(t) becomes a piecewise function defined for patient i, interval j
            # λi,j= λj exp(xi β)
            # creating a matrix n_patients X n_intervals
            # lambda_ = pm.Deterministic('lambda_', lambda0_per_patient * attr_hazard)
            # for efficiency, do not name
            lambda_ = lambda0_per_patient * attr_hazard

            # death can be approximated as a Poisson distribution per-interval j, patient i,
            # with mean (expected value) exposure*λi,j
            # mu = pm.Deterministic('mu', exposure * lambda_)
            # for efficiency, do not name
            mu = exposure * lambda_
            pm.Poisson(name='obs', mu=mu, observed=death)
            # so obs is the final product, a matrix of (patients X intervals) with a Poisson distribution
            # of expected occurrence of the event in each cell, where the expected value depends on
            # - the gamma-distributed abstract λ0
            # - the factors X beta

    def vars_of_interest(self):
        return super().vars_of_interest() + [self.pymc_model.named_vars["lambda0 per strata"]]

    def empirical_function(self,
                           function_type: str,
                           result: BayesianSurvival.Result,
                           data: pd.DataFrame,
                           alpha: float = 0.05) -> pd.DataFrame:

        new_strata, new_covariate_data = self.prepare_new_data(data)
        new_n_patients = len(data)

        trace = result.get_trace()

        # shape: (n_samples, n_strata, n_intervals)
        lambda0_per_strata = trace["lambda0 per strata"]
        n_samples = lambda0_per_strata.shape[0]
        # shape: n_samples, new_n_patients, n_intervals
        lambda0_per_patient = lambda0_per_strata[:, new_strata, :]

        # shape: new_n_patients x n_samples
        attr_hazard_pred = np.exp(
            self.covariate_set.covariates_dot_coefficients(covariate_data=new_covariate_data,
                                                           coefficients=trace)
        )
        # reshape to n_samples, new_n_patients, 1
        attr_hazard_pred = np.expand_dims(attr_hazard_pred.T, axis=2)
        # shape: n_samples, new_n_patients, n_intervals
        lambda_ = lambda0_per_patient * attr_hazard_pred

        def cum_hazard(hazard):
            return (self.interval_lengths * hazard).cumsum(axis=-1)

        def survival(hazard):
            return np.exp(-cum_hazard(hazard))

        percentiles = 100 * np.array([alpha / 2., 0.5, 1. - alpha / 2.])

        if function_type == "survival":
            func = survival
            zero = 1.0
        elif function_type == "cumulative hazard":
            func = cum_hazard
            zero = 0.0
        else:
            raise ValueError("Unknown function type " + function_type)

        # removing dim 0, shape: new_n_patients, n_intervals
        mean = func(np.mean(lambda_, axis=0))
        # along dim 0, shape: len(percentiles), new_n_patients, n_intervals
        hpd = np.percentile(func(lambda_), percentiles, axis=0)
        # split at first dim (one python variable for each percentile)
        hpd_lower, median, hpd_upper = hpd[0], hpd[1], hpd[2]

        function_df = pd.DataFrame()

        for patient in range(new_n_patients):
            function_df = function_df.append(pd.DataFrame({
                "patient": data.index[patient],
                "x": self.interval_bounds,
                "mean": np.insert(mean[patient], 0, zero),
                "median": np.insert(median[patient], 0, zero),
                "hpd_lower": np.insert(hpd_lower[patient], 0, zero),
                "hpd_upper": np.insert(hpd_upper[patient], 0, zero)
            }))

        return function_df

    def sample_ppc(self,
                   result: "BayesianSurvival.Result",
                   data: pd.DataFrame,
                   n_samples=500,
                   draws_per_sample=1,
                   seed=None) -> pd.DataFrame:

        new_strata, new_covariate_data = self.prepare_new_data(data)

        n_intervals = len(self.interval_lengths)
        new_n_patients = len(data)

        trace = result.get_trace()[result.n_burn:]

        # I have no idea about the implications of this.
        # Just think that we add variables, which we shouldn't do to the original model but to a copy
        ppc_model = copy.deepcopy(self.pymc_model)
        with ppc_model:
            lambda0_per_strata = ppc_model.named_vars["lambda0 per strata"]
            lambda0_per_patient = pick_rows(lambda0_per_strata, new_strata)
            new_hazard = T.exp(self.covariate_set.covariates_dot_coefficients(covariate_data=new_covariate_data,
                                                                              mode="theano"))
            new_lambda_ = pm.Deterministic("new lambda_", lambda0_per_patient * new_hazard)
            new_mu = pm.Deterministic("new mu", new_lambda_ * self.interval_lengths)  # multiply row-wise
            new_x = pm.Poisson(name="new x", mu=new_mu, shape=(new_n_patients, n_intervals))

            n_samples_total = n_samples * draws_per_sample

            ppc_dict = pm.sample_ppc(trace,
                                     vars=[new_x],
                                     samples=n_samples,
                                     size=draws_per_sample)
            sampled_x = ppc_dict["new x"].reshape(-1, new_n_patients, n_intervals)
            # flatten dim 1 (n_samples) and 2 (size)
            # After reshape: first dim: sample size; second dim: len(test_hazard); third dim: interval

            sampled_lambda = trace["lambda0"] * new_hazard
            # same dimensions (after the multiplication) as sampled_x

            survival_df = pd.DataFrame()

            for patient in range(sampled_x.shape[1]):
                times = np.zeros(n_samples_total)
                status = np.zeros(n_samples_total, dtype=np.int)
                # print("Patient {}:".format(patient))
                # print(sampled_x[:, patient, :])

                for sample in range(n_samples_total):
                    for interval in range(n_intervals):
                        interval_length = self.interval_lengths[interval]
                        if sampled_x[sample, patient, interval] == 0:
                            times[sample] += interval_length
                        else:
                            # Within the interval, we have per definition a constant hazard.
                            # A constant hazard function has a survival function which is the exponential distribution
                            # with λ (the distribution's parameter) = λ (the constant hazard) and expected value 1/λ
                            constant_hazard = sampled_lambda[sample, patient, interval]
                            within_interval_survival = stats.expon.rvs(scale=1.0 / constant_hazard)
                            times[sample] += min(within_interval_survival, interval_length - 1)
                            status[sample] = 1
                            break
                survival_df = survival_df.append(
                    pd.DataFrame({"patient": patient,
                                  "time": times,
                                  "status": status}),
                    ignore_index=True
                )
        return survival_df

    def transform_start_value(self,
                              basename,
                              prefixed_name,
                              value):
        if basename == 'lambda0 per strata_log__':
            needed_shape = (len(self.data_set.strata_levels), len(self.interval_lengths))
            if value.shape != needed_shape:
                ratio = np.asarray(needed_shape) / np.asarray(value.shape)
                value = zoom(value, ratio)
        return super().transform_start_value(basename, prefixed_name, value)

    def _calculate_intervals_fixed_length(self):
        n_patients = len(self.data_set.survival_data)
        patients = np.arange(n_patients)

        # interval length (days)
        interval_length = 15
        interval_bounds = np.arange(0, self.data_set.survival_data["time"].max()
                                    + interval_length + 1, interval_length)
        n_intervals = interval_bounds.size - 1
        interval_lengths = np.repeat(interval_length, n_intervals)

        # plot_death_distribution(self.survival_data, interval_bounds)

        # last_period[i] = last period in which patient i was at risk (died or was censored)
        last_period = np.floor((self.data_set.survival_data["time"] - 0.01) / interval_length).astype(int)
        # death[i,j] = 1 if patient i died in interval j, else 0
        death = np.zeros((n_patients, n_intervals))
        death[patients, last_period] = self.data_set.survival_data["status"]

        # exposure[i,j] = time that patient i was at risk in interval j
        # "greater_equal.outer" = apply greater_equal to all pairs (a,b) (numpy.ufunc.outer.html)
        # -> create array [i,j] where i=interval_length if <= last period, else 0;
        # then, get more precise for last interval
        exposure = np.greater_equal.outer(
            self.data_set.survival_data["time"], interval_bounds[:-1]) * interval_length
        exposure[patients, last_period] = self.data_set.survival_data["time"] - interval_bounds[last_period]
        return interval_lengths, interval_bounds, death, exposure

    def _calculate_intervals_variable_length(self):
        n_patients = len(self.data_set.survival_data)
        patients = np.arange(n_patients)

        # interval bounds derived from event times
        bounding_timepoints = self.data_set.survival_data.loc[self.data_set.survival_data["status"] == 1, "time"]
        # bounding_timepoints = self.survival_data["time"]
        interval_bounds = [0] + list(bounding_timepoints)
        if np.max(interval_bounds) < self.data_set.survival_data["time"].max():
            interval_bounds.append(self.data_set.survival_data["time"].max())
        interval_bounds = np.sort(np.unique(interval_bounds))

        # reduce intervals to a manageable amount if >50
        interval_bounds = reduce_intervals(interval_bounds, 50)

        n_intervals = interval_bounds.size - 1
        interval_lengths = np.diff(interval_bounds)
        #print("Interval lengths ({}): {}".format(n_intervals, interval_lengths))

        # boolean array of touched periods
        touched_periods = np.greater.outer(self.data_set.survival_data["time"], interval_bounds[:-1])
        untouched_periods = ~touched_periods  # type: np.ndarray

        # index array of last period
        # touched_periods_with_final_period=np.hstack([touched_periods, np.full((n_patients, 1), False)])
        last_period = np.where(np.any(untouched_periods, axis=1),
                               np.argmax(untouched_periods, axis=1),
                               np.full(n_patients, n_intervals)) - 1

        # death[i,j] = 1 if patient i died in interval j, else 0
        death = np.zeros((n_patients, n_intervals))
        death[patients, last_period] = self.data_set.survival_data["status"]

        # exposure[i,j] = time that patient i was at risk in interval j
        # "greater_equal.outer" = apply greater_equal to all pairs (a,b) (numpy.ufunc.outer.html)
        # -> create array [i,j] where i=interval_length if <= last period, else 0;
        # then, get more precise for last interval
        exposure = touched_periods * interval_lengths
        exposure[patients, last_period] = self.data_set.survival_data["time"] - interval_bounds[last_period]
        # print(pd.DataFrame(exposure))

        return interval_lengths, interval_bounds, death, exposure