from typing import List, Union, Tuple, Sequence, Dict, Callable

import numpy as np
import pandas as pd

import pymc3 as pm
import pymc3.backends
import pymc3.backends.base
import pymc3.backends.tracetab
from theano import tensor as T


class DataSet:
    """Bundles survival, covariate and strata data used by a model

    :strata: A pd.Series containing the stratum index for each sample
    :strata_levels:
        A data frame containing one column "stratum" containing the index used in strata,
        and further columns corresponding to the columns defining the strata,
        where each line contains one unique cominbation of values in these columns.
    """

    def __init__(self,
                 data: pd.DataFrame,
                 survival_data: pd.DataFrame,
                 survival_columns: Sequence[str],
                 strata: pd.Series,
                 strata_levels: pd.DataFrame):
        self.data = data
        self.survival_data = survival_data
        self.survival_columns = survival_columns
        self.strata = strata
        self.strata_levels = strata_levels
        self.n_patients = len(self.survival_data)


class CovariateSet:
    """Bundles RVGroups used by a model.

    Provide a dict of type: Group containing the covariate groups.
    See CovariateSet.Group.
    Please do not include a "constant" group, but only n_samples and add each constant later with add_constant.

    """

    def __init__(self,
                 groups: list,
                 n_samples: int):
        self.groups = groups
        self.n_samples = n_samples
        self.print = False

        # TODO: translation of one_hot betas to level to ref-level

    constant = "constant"
    numerical = "numerical"
    boolean = "boolean"
    categorical = "categorical"
    tensorvariable = "tensorvariable"

    class Group:
        """Hold a group of covariates, their coefficient and a list of extra information

        Typically, the covariates in the same group are of the same type.
        The type of covariates is flexible: Can be numpy/pandas arrays, or TensorVariables.
        This class does not enforce either, provide what you need back.
        Dimension of covariates is assumed (n,k) where n is the number of samples and k is the numer of features
        for the given type.
        Coefficients are usually one TensorVariable object containing a vector of coefficients of dimension (k,)
        One other case is a per-sample coefficient matrix (n,k). In this case, please pass ("n","k")
        as coefficients_shape parameter.
        Ids are the column names, or, in case of categorical type, tuples of (column name, level).

        """

        def __init__(self,
                     type_: str,
                     covariates,
                     coefficients: Union[None, T.TensorVariable],
                     ids: Sequence,
                     coefficients_shape: Tuple = ("k",)):
            self.type_ = type_
            self.covariates = covariates
            self.coefficients = coefficients
            self.ids = ids
            self.coefficients_shape = coefficients_shape
            if self.coefficients_shape != ("k",) and self.coefficients_shape != ("n", "k"):
                raise ValueError("Coefficients_shape must be either (k,) or (n,k)")

    @staticmethod
    def types():
        return [CovariateSet.constant,
                CovariateSet.numerical,
                CovariateSet.boolean,
                CovariateSet.categorical,
                CovariateSet.tensorvariable]

    def coefficient(self,
                    id: str,
                    level: str = None):
        """Returns the coefficient variable for the covariate with the given id.

        In case of a categorical covariate, you must specify the level
        (categorical covariates have one reference level, and all other levels have one coefficient in relation
        to the reference level)
        For boolean or numerical covariates, the level parameter is ignored:
            In case of a boolean covariate, the reference level is False and the level is True.
            In case of a numerical covariate, the coefficient refers to an increment of 1.

        :param id:
            Covariate id (was: column name in data frame)
        :param level:
            For categorical covariates, specify the level for which you request the coefficient.
            If you specify the reference level, you will receive None.
        :return:
            A T.TensorVariable of shape (1,)
        """
        for group in self.groups:
            if group.ids:
                for i, group_id in zip(range(len(group.ids)), group.ids):
                    if group.type_ == self.categorical:
                        if group_id[0] == id and group_id[1] == level:
                            return group.coefficients[i]
                    else:
                        if group_id == id:
                            return group.coefficients[i]

    def add_constant(self,
                     rv: T.TensorVariable,
                     name: str = None):
        """ Add a constant to this covariate group

        :param rv:
            Coefficient random variable. (shape=1)
        :param name: the name
        """
        group = None
        for g in self.groups:
            if g.type_ == CovariateSet.constant:
                group = g
                break
        if group is None:
            self.groups.append(CovariateSet.Group(CovariateSet.constant, None, None, []))

        if not name:
            name = rv.name

        if group.ids:
            group.coefficients = T.concatenate([group.coefficients, rv])
        else:
            group.coefficients = rv
        group.ids.append(name)
        group.covariates = T.ones((self.n_samples, len(group.ids)))

    def covariates_dot_coefficients(self,
                                    covariate_data=None,
                                    coefficients=None,
                                    mode: str = "numpy"):
        """Implements the "xβ" part of λ(t)=λ0(t)exp(xβ)

        Per default, uses the stored covariates and coefficients;
        for post sampling analysis, you may replace either.
        If mode is "numpy", used numpy methods, if "theano", uses theano ops.
        :covariate_data:
            None (using the current data), or a list
            of tuples (type, data) data numpy/pandas or theano with shape (patients, features for type)
            where this list must match exactly the groups of this set (create the list with extract_covariates())
        :coefficients:
            Can be None (using the current coefficients), or a trace (using the posterior distribution)
        :mode: "numpy" or "theano"
        :return:
            A matrix (numpy of theano). Shape depends a bit on input and is difficult to control with
            theano vars. It will be (n_patients,) or (1, n_patients), or, for a trace, (n_patients, n_samples)
        """
        if not covariate_data:
            covariate_data = []

        if isinstance(coefficients, pm.backends.base.MultiTrace):
            has_trace = True
        elif coefficients is None:
            has_trace = False
        else:
            raise ValueError("Coefficients must be None or a trace")

        if mode == "theano":
            ops = {
                "dot": T.dot,
                "sum": T.sum,
                "concat_stack": T.stack,
                "zeros": T.zeros
            }
        else:
            ops = {
                "dot": np.dot,
                "sum": np.sum,
                "concat_stack": np.concatenate,
                "zeros": np.zeros
            }

        def dot_products():
            for i, group in enumerate(self.groups):
                if group.ids:

                    if has_trace:
                        coef = coefficients[group.coefficients.name]
                        # The trace returns shape (n_tracesamples, shape of coef)
                        if group.coefficients_shape == ("k",):
                            # transpose so that we multiply
                            # (n_patients, n_features) X (n_features, n_tracesamples) -> (n_patients, n_tracesamples)
                            coef = coef.T
                        # else trace is (n_tracesamples, n, k), leave that as is for elem-wise multiplication
                    else:
                        coef = group.coefficients

                    cova = covariate_data[i][1] if covariate_data else group.covariates

                    if group.coefficients_shape == ("k",):
                        # covariates shaped (n,k), coefficients vector of (k,) -> dot
                        result = ops["dot"](cova, coef)
                    else:
                        # shape (n,k) for both covariate and coefficients -> elem-wise multiplication and sum
                        product = cova * coef
                        if has_trace:
                            # reshape (n_tracesamples, n, k) to (n, k, n_tracesamples), so we sum to (n, n_tracesamples)
                            product = np.moveaxis(product, 0, 2)
                        result = ops["sum"](product, axis=1)

                    if self.print:
                        result = T.printing.Print("Shape of result of {}".format(group.ids), ["__str__", "shape"])(result)

                    # with theano, we keep the result as is (n_patients,) and use T.stack.
                    if mode != "theano":
                        # Normally we get (n_patients,) which we reshape to (n_patients,1) for concatenation.
                        # from a trace, we got (n_patients, n_tracesamples),
                        # which becomes (n_patients, 1, n_tracesamples)
                        result = np.expand_dims(result, axis=1)
                    yield result

        dots = list(dot_products())
        if not dots:
            return ops["zeros"]((self.n_samples,))
        return ops["sum"](ops["concat_stack"](dots, axis=1), axis=1)

        # if mode == "theano":
        #    return T.sum(T.stack(list(dots), axis=1), axis=1)
        # else:
        #    return np.sum(np.concatenate(list(dots), axis=1), axis=1)

    def extract_covariate_data(self,
                               data: pd.DataFrame) -> List:
        """From a given data frame with fresh data, extract the data needed for the covariates in this set.

        :return: a list of tuples (type, pd.DataFrame)"""

        def hazard_data(group) -> pd.Series:
            if group.type_ == CovariateSet.categorical:
                for column, cat in group.ids:
                    yield pd.Series(data[column] == cat,
                                    dtype=np.int8,
                                    name=column + "_" + str(cat))
            else:
                for column in group.ids:
                    yield data[column]

        def hazard_dfs():
            for group in self.groups:
                if not group.ids:
                    yield group.type_, None
                yield group.type_, pd.concat(list(hazard_data(group)), axis=1)

        return list(hazard_dfs())

    def summary_df(self,
                   data_set: DataSet):

        covariate_names = []
        value_names = []
        subgroup_ns = []
        plain_varnames = []
        formatted_varnames = []
        n = len(data_set.data)

        for group in self.groups:
            if not group.ids:
                continue

            plain_varnames.extend([group.coefficients.name] * len(group.ids))
            formatted_varnames.extend(
                pm.backends.tracetab.create_flat_names(group.coefficients.name, np.atleast_1d(len(group.ids)))
            )

            if group.type_ == CovariateSet.categorical:
                covariate_names_group = [pair[0] for pair in group.ids]
                value_names_group = [pair[1] for pair in group.ids]
            else:
                covariate_names_group = group.ids
                if group.type_ == CovariateSet.numerical:
                    value_name = "<continuous>"
                elif group.type_ == CovariateSet.boolean:
                    value_name = str(True)
                elif group.type_ == CovariateSet.constant or group.type_ == CovariateSet.tensorvariable:
                    value_name = "<continuous>"
                else:
                    raise ValueError("Unknown type " + group.type_)
                value_names_group = [value_name] * len(group.ids)

            covariate_names.extend(covariate_names_group)
            value_names.extend(value_names_group)

            if group.type_ == CovariateSet.constant or group.type_ == CovariateSet.tensorvariable:
                subgroup_ns.extend([n] * len(group.ids))
            else:
                subgroup_ns.extend(
                    [np.sum(~data_set.data[column].isnull())
                     for column in covariate_names_group]
                )

        return pd.DataFrame({
            "covariate.name": covariate_names,
            "variable name": plain_varnames,
            "formatted variable name": formatted_varnames,
            "covariate.value": value_names,
            "n": n,
            "subgroup_n": subgroup_ns
        })


class CovariateBuilder:

    def __init__(self,
                 data_set: DataSet,
                 covariates_with_distribution: Dict[str, Callable] = None,
                 covariates_fully_observed: Sequence[str] = None,
                 ref_level_dict: Dict = None):
        if covariates_fully_observed is None:
            covariates_fully_observed = []
        if covariates_with_distribution is None:
            covariates_with_distribution = {}

        self.data_set = data_set
        self.covariates_with_distribution = covariates_with_distribution
        self.covariates_fully_observed = covariates_fully_observed
        self.ref_level_dict = ref_level_dict

    def extract_covariates(self):
        # Sort and build covariate data
        covariate_columns = list(self.covariates_with_distribution.keys()) + self.covariates_fully_observed
        covariates = self.data_set.data[covariate_columns]

        object_columns = covariates.select_dtypes(["object"])
        object_boolean_columns = []
        object_categorical_columns = []
        for column in object_columns.columns:
            series = object_columns[column]
            if np.all(series.isin([True, False, np.NaN, None])) or np.all(series.isin([1., 0., np.NaN, None])):
                object_boolean_columns.append(series.astype("float"))
            else:
                print("Object column " + column + ": Using as categorical data")
                object_categorical_columns.append(series.astype("category"))

        covariates_numerical = covariates.select_dtypes([np.number])
        covariates_boolean = pd.concat([covariates.select_dtypes(["bool"])] +
                                       object_boolean_columns, axis=1)
        covariates_categorical = pd.concat([covariates.select_dtypes(["category"])] +
                                           object_categorical_columns, axis=1)
        covariate_constant = np.ones((self.data_set.n_patients, 1))

        covariates = pd.concat([covariates_numerical,
                                covariates_boolean,
                                covariates_categorical], axis=1)  # type: pd.DataFrame

        return covariates, covariates_numerical, covariates_boolean, covariates_categorical, covariate_constant

    def tensor_var_for_covariate(self,
                                 column: str,
                                 data):
        if self.covariates_with_distribution and column in self.covariates_with_distribution:
            generator = self.covariates_with_distribution[column]
            # call generator function which generates a RV for this model with given observed data
            var = generator(self.data_set.data, name=column, observed=data)
        else:
            var = T.as_tensor_variable(data)
        return var.reshape((self.data_set.n_patients, 1))  # 1D to column vector

    def rvs_ids(self,
                covariates):
        """For a given data frame of numerical or boolean covariates, return RVs and ids

        :returns: A tuple of RVs (theano tensor variables) and a matching list of ids (column names)"""
        if isinstance(covariates, pd.Series):
            df = covariates.to_frame()
        if covariates is None or covariates.shape[1] == 0:
            return None, None

        rvs = []
        for column in covariates.columns:
            rv = self.tensor_var_for_covariate(column, covariates[column])
            rvs.append(rv)
        rvs = T.concatenate(rvs, axis=1)
        # if len(df.columns) == 1:  # 1D to column vector
        # rvs = rvs.dimshuffle((0, 'x'))
        return rvs, list(covariates.columns)

    def rvs_ids_categorical(self,
                            covariates):
        """For a given data frame of categorical covariates, return one-hot RVs and ids

        :returns:
            A tuple of RVs (theano tensor variables transformed to one-hot, ref level omitted)
            and a matching list of tuples (column name, level)
        """
        if isinstance(covariates, pd.Series):
            covariates = covariates.to_frame()
        if covariates is None or covariates.shape[1] == 0:
            return None, None

        rvs_categorical = []
        category_one_hot_dict = {}
        categories_one_hot = []
        vars_one_hot = []
        for column in covariates.columns:
            series = covariates[column]

            if self.ref_level_dict and column in self.ref_level_dict:
                ref_level = self.ref_level_dict[column]
                if not series.cat.categories.contains(ref_level):
                    raise ValueError("Reference level {} not contained in categories ({} )of {}".format(
                        ref_level, series.cat.categories.values, column
                    ))
            else:
                ref_level = series.cat.categories[0]
            # print("Ref level for {}: {}".format(column, ref_level))
            levels_mask = series.cat.categories == ref_level  # type: np.ndarray
            # ref_level_idx = np.nonzero(levels_mask)[0][0]
            levels = series.cat.categories
            codes_without_ref = np.arange(series.cat.categories.size)[~levels_mask]
            levels_without_ref = levels[~levels_mask]

            # To codes. Mask NA values.
            is_na = series.isnull()
            integer_codes = series.cat.codes
            if is_na.any():
                integer_codes = np.ma.array(integer_codes, mask=is_na)

            # one RV for the variable
            rv = self.tensor_var_for_covariate(column, integer_codes)

            # and a one-hot matrix for actual use, as a deterministic transformation, omitting the reference level

            one_hot = T.concatenate([T.eq(rv, code)
                                     for code in codes_without_ref], axis=1)

            rvs_categorical.append(rv)
            vars_one_hot.append(one_hot)
            # build nested dict: {column -> {dict: category -> index in covariate_vars_one_hot/beta_categorical}}
            category_one_hot_dict[column] = {
                cat: i + len(categories_one_hot) for i, cat in enumerate(levels_without_ref)
            }
            # build list:  [ (column, category) ]
            # with indexes corresponding to index in covariate_vars_one_hot/beta_categorical
            for i, cat in enumerate(levels_without_ref):
                categories_one_hot.append((column, cat))

        vars_one_hot = T.concatenate(vars_one_hot, axis=1) if vars_one_hot else None

        return vars_one_hot, categories_one_hot
