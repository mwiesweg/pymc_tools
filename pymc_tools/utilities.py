from typing import List, Dict, Callable, Union, Tuple, Sequence
import os
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

import pymc3 as pm
from pymc3.model import TransformedRV
from pymc3.util import update_start_vals
from theano import tensor as tt
from theano.gof.fg import MissingInputError


def rv_generator(func: Callable,
                 **kwargs):
    """Allows to specify a covariate that has missing values and a known distribution (prior knowledge)

    For use as a value in the covariates_with_distribution dict of multivariate_model():
    covariates_with_distribution = { ..., "my_covariate": rv_generator(pm.Bernoulli, p=0.85), ... }

    :param func:
        Callable that generates a pymc3 Distribution.
        Usually, this is one of the provided Distribution classes.
    :param kwargs:
        Parameters needed to fully specify the distribution
    :return:
        A callable which is used when processing the covariate data.
    """
    return lambda df, name, observed: func(name=name, observed=observed, **kwargs)


def diagnose_model_specification(model=None, start=None):
    """Tool to locate the problem of "ValueError: Bad initial energy: nan. The model might be misspecified." """

    model = pm.modelcontext(model)

    if start is None:
        start = {}

    # updates start in-place
    update_start_vals(start, model.test_point, model)
    print("Model specification:")

    for RV in model.basic_RVs:
        print("test point and logp of {}:".format(RV.name))
        print(start[RV.name] if RV.name in start else RV.tag.test_value)
        try:
            print("logp: {}".format(RV.logp(start)))
        except MissingInputError as e:
            print(e)

    print("Model logp: {}".format(model.logp(start)))


# Numpy utilities


def as_column_vector(x):
    """With x as a list or np array: Make it a column vector"""
    return np.asarray(x).reshape((len(x), 1))


# Numpy or theano compatible


def pick_rows(x,
              row_indices,
              second_dim: int=None
              ):
    """From a l x i matrix, return a p x i by picking p rows from x using the p-sized vector row_indices

    :param x: ndarray-like-indexable array
    :param row_indices:
        Vector of length p, giving the chosen row index for each column
    :param second_dim:
        The size i of the second dimension of x and the return value.
        If None, uses x' shape; You only need to specify this if the shape is not accessible at compile time.
    :return: ndarray/TensorVariable of dimension 1xc
    """
    row_indices = row_indices.ravel()  # make one-dim

    if not second_dim:
        if type(x) is tt.TensorVariable:
            raise ValueError("If x is a TensorVariable, its shape is not accessible at compile time. "
                             "You must specify second_dim.")
        else:
            second_dim = x.shape[0]

    return x[np.ix_(row_indices, np.arange(second_dim))]


def pick_columns(x,
                 column_indices,
                 first_dim: int=None
                 ):
    """From a i x c matrix, return a l x p matrix by picking p columns from x using the p-sized vector column_indices

    :param x: ndarray-like-indexable array
    :param column_indices:
        Vector of length p, giving the chosen column index for each row
    :param first_dim:
        The size i of the first dimension of x and the return value.
        If None, uses x' shape; You only need to specify this if the shape is not accessible at compile time.
    :return: ndarray/TensorVariable of dimension cx1
    """
    column_indices = column_indices.ravel()  # make one-dim

    if not first_dim:
        if type(x) is tt.TensorVariable:
            raise ValueError("If x is a TensorVariable, its shape is not accessible at compile time. "
                             "You must specify first_dim.")
        else:
            first_dim = x.shape[0]

    return x[np.ix_(np.arange(first_dim), column_indices)]


# Theano utilities


def theano_expand_dims(x: tt.TensorVariable, axis=-1):
    """With x from theano: Add a 1-sized dimension at index "dim".
    """
    pattern = [i for i in range(x.type.ndim)]
    if axis < 0:
        if x.type.ndim == 0:
            axis = 0
        else:
            axis = axis % x.type.ndim + 1
    pattern.insert(axis, 'x')
    y = x.dimshuffle(pattern)
    return y


def theano_multiply_columnwise(matrix: tt.TensorVariable,
                               vector: tt.TensorVariable):
    # cannot do static checks here
    return matrix * tt.extra_ops.repeat(vector, matrix.shape[1], axis=1)


def pereira_sternberg(x,
                      theta_null,
                      axis=0):
    """Returns the smallest α for which θ0 is not contained in the 1-α HPD

    :param x: The data (array-like)
    :param theta_null: The value just not contained in the 1-α HPD
    :param axis: if x is 2D, along which axis to proceed
    :return: 0<α<1, or 1D ndarray if x is 2D
    """

    if x.ndim == 2:
        return np.apply_along_axis(pereira_sternberg, axis=axis, arr=x, theta_null=theta_null)
    elif x.ndim > 2:
        raise ValueError(">2D arrays not supported")

    # this employs brute-force solving, perhaps nothing for computer scientists, but fine for me

    def try_hpds(starting, alphas):
        last_in = starting
        first_out = 1
        for alpha in alphas:
            try:
                hpd = pm.stats.hpd(x, alpha)
            except ValueError:
                continue
            if hpd[0] < theta_null < hpd[1]:
                last_in = alpha
            else:
                first_out = alpha
                break
        return last_in, first_out

    a_1, a_2 = try_hpds(0, np.arange(0, 1, step=0.01))
    a_1, a_2 = try_hpds(a_1, np.linspace(a_1, a_2, num=100))

    return a_2

def plot_death_distribution(df: pd.DataFrame,
                            interval_bounds: np.ndarray):
    fig, ax = plt.subplots(figsize=(8, 6))

    ax.hist(df[df.event == 1].time.values, bins=interval_bounds,
            color="red", alpha=0.5, lw=0,
            label='Uncensored')
    ax.hist(df[df.event == 0].time.values, bins=interval_bounds,
            color="blue", alpha=0.5, lw=0,
            label='Censored')

    ax.set_xlim(0, interval_bounds[-1])
    ax.set_xlabel('Days')

    ax.set_yticks([0, 1, 2, 3])
    ax.set_ylabel('Number of observations')

    ax.legend()
    plt.show()


def prepare_directory(directory, *args):
    directory = directory.format(*args)
    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory


def make_survival_dataframe(df: pd.DataFrame,
                            survival_columns: Sequence[str]) -> pd.DataFrame:
    if not all(df.columns.contains(col) for col in survival_columns):
        raise ValueError("Survival columns {} not found in data frame!".format(survival_columns))

    time = df[survival_columns[0]]
    status = df[survival_columns[1]].astype("int")

    if not np.all(time > 0):
        raise ValueError("All survival times must be > 0 ({})".format(survival_columns))
    if not np.all(status.isin([0, 1])):
        raise ValueError("Survival status must be binary (logical or 0/1)({})".format(survival_columns))

    return pd.DataFrame(
        {
            "time": time,
            "status": status
        }
    )


def make_strata(df: pd.DataFrame,
                strata_columns: Union[None, List[str]]) -> Tuple[pd.Series, pd.DataFrame]:
    if strata_columns is None:
        strata_columns = []
    if strata_columns:
        # get data frame with those columns used for strata
        strata_df = df[strata_columns]
        # get unique combinations of strata columns, and label them
        strata_levels = strata_df.drop_duplicates().reset_index(drop=True) \
            .reset_index().rename(columns={"index": "stratum"})
    else:
        strata_levels = pd.DataFrame({"stratum": 0}, index=[0])

    return strata(df, strata_levels), strata_levels


def strata(df: pd.DataFrame,
           strata_levels: pd.DataFrame) -> pd.Series:
    # get strata_columns, names of data columns determining the stratum (column "stratum" gives the stratum index
    strata_columns = [column for column in strata_levels.columns if column != "stratum"]
    if not strata_columns:
        return pd.Series(0, index=np.arange(df.shape[0]), dtype=np.int, name="stratum")

    return (df.
            reset_index().  # store patient index as "index"
            merge(strata_levels, how="left", on=strata_columns).  # merge on unique combinations of strata columns
            set_index("index").  # restore patient index
            reindex(index=df.index)  # sort patient as before (was sorted on merge columns)
            ["stratum"]  # extract stratum index - now in correct order
            )


def reduce_intervals(bounds, limit):
    """Reducces the given interval bounds so that not more than limit intervals (limit+1 bounds) remain"""
    while len(bounds) > limit+1:
        diffs = np.diff(bounds)
        min_diff = min(diffs)
        mask = np.concatenate([[True], diffs != min_diff])
        bounds = bounds[mask]
    return bounds


def vars_and_transforms(vars_):
    """
    Extends the list of RVs with their transformed RVs
    :param vars_: a list of RVs
    :return: the RVs and, if transformed, the corresponding transformed RVs
    """
    return vars_ + [var.transformed for var in vars_ if isinstance(var, TransformedRV)]


def create_interpolation(name, 
                         points,
                         resolution=100,
                         **kwargs):
    """
    Creates an "Interpolated" distribution for the given trace points
    :param name: RV name
    :param points: trace points (one-dimensional)
    :param resolution: Number of histogram bins used to compute empirical PDF
    :param kwargs: Passed to created pymc3 distribution (can include "observed" parameter)
    :return: an instance of pm.Interpolated
    """
    hist, edges = np.histogram(points, resolution, density=True)
    middles = edges[:-1] + np.diff(edges) / 2
    return pm.Interpolated(name=name,
                           x_points=middles,
                           pdf_points=hist,
                           **kwargs)


def as_interpolation(trace, 
                     varname,
                     resolution=100):
    """
    Creates an "Interpolated" distribution for the given varname from its posterior distribution in trace
    :param trace: A MultiTrace
    :param varname: Variable name
    :param resolution: Number of histogram bins used to compute empirical PDF
    :return: an instance of pm.Interpolated
    """
    # shape is (number of samples, <shape of original RV>)
    points = trace[varname]

    if points.ndim == 1:
        return create_interpolation(varname, points)
    elif points.ndim == 2:

        def sub_interpolations():
            for i in range(points.shape[1]):
                sub_points = points[:, i]
                yield create_interpolation(name="{}{}".format(varname, i),
                                           points=sub_points, 
                                           resolution=resolution)

        return pm.Deterministic(varname, tt.stack(list(sub_interpolations())))
    else:
        raise ValueError("Tensor {} has {} dimensions. Only 1 or 2 dimensions are supported".
                         format(varname, points.ndim))


def as_interpolations(trace, 
                      varnames,
                      resolution=100):
    """
    Creates "Interpolated" distributions for the given varnames from the posterior distributions in trace
    :param trace: A MultiTrace
    :param varnames: Variable names
    :param resolution: Number of histogram bins used to compute empirical PDF
    :return: A dictionary of variable name -> RV
    """
    return {name: as_interpolation(trace, name, resolution=resolution) for name in varnames}
