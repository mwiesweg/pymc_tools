from distutils.core import setup

setup(
    name='pymc_tools',
    version='0.1',
    packages=['pymc_tools'],
    url='',
    license='GPLv3',
    author='Marcel Wiesweg',
    author_email='marcel.wiesweg@uk-essen.de',
    description='Tools for some applications of PyMC3',
    install_requires=[
        "pandas",
        "numpy",
        "scipy",
        "pymc3",
        "theano",
        "feather"
    ]
)
