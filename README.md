# README #

### What is this repository for? ###

* Survival analysis models using PyMC3
* 0.1

### How do I get set up? ###

* Python 3.6
* PyMC3 >= 3.3

### Who do I talk to? ###

* Marcel Wiesweg <marcel.wiesweg@uk-essen.de>